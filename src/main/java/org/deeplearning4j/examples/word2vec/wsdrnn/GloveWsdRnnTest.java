package org.deeplearning4j.examples.word2vec.wsdrnn;

import org.apache.commons.io.FilenameUtils;
import org.deeplearning4j.datasets.iterator.AsyncDataSetIterator;
import org.deeplearning4j.datasets.iterator.DataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 */
public class GloveWsdRnnTest {

    private static GloveWsdRNN gloveWsdRNN = new GloveWsdRNN();
    public static String outputDirectory = "rnnResultsDir";

    public static final String senseLabelsDirectory
            = "senseLabels/";

    public static void main(String[] args) throws Exception {

        if (!new File(outputDirectory).exists()) {
            new File(outputDirectory).mkdir();
        }

        Map<String, List<String>> testCases = gloveWsdRNN.testCases(
                                                        gloveWsdRNN.TEST_DATA_PATH);


        System.out.println(testCases);

        final int batchSize = 50;                 // Number of examples in each minibatch
        final int vectorSize = 50;                // Size of the word vectors. 50 in GLOVE. CHANGE THIS WHEN EXPERIMENTING WITH WORD VECTORS

        final int truncateReviewsToLength = 100;  // Truncate reviews with length (# words) greater than this

        WordVectors wordVectors = WordVectorSerializer.loadTxtVectors(
                new File(gloveWsdRNN.WORD_VECTORS_PATH)
        );

        for (Map.Entry<String, List<String>> posTypeAndTargetWordNames : testCases.entrySet()) {

            String posType = posTypeAndTargetWordNames.getKey();

            List<String> targetWordsXmlNames = posTypeAndTargetWordNames.getValue();

            for (String targetWordXmlName : targetWordsXmlNames) {

                try {

                    if (!gloveWsdRNN.isModelExists(posType, targetWordWithoutExtension(targetWordXmlName))) {
                        System.err.println("pos: " + posType + " , targetWord : " + targetWordWithoutExtension(targetWordXmlName));
                        throw new IllegalStateException("Model must exist before running test. Run "
                                + GloveWsdRnnTrain.class + " before testing...");
                    }

                    String targetWordXmlPath = posType + "/" + targetWordXmlName;
                    String targetWordKeyPath = targetWordXmlPath.replace("xml", "key");

                    WsdIterator wsdIter = new WsdIterator(gloveWsdRNN.TEST_DATA_PATH, targetWordXmlPath,
                            targetWordKeyPath, wordVectors,
                            100, truncateReviewsToLength,
                            senseLabelsDirectory + "/" + gloveWsdRNN.senseLabelFileName(targetWordXmlPath));


                    FileWriter writer = new FileWriter(FilenameUtils.concat(outputDirectory, targetWordWithoutExtension(targetWordXmlName) + ".result"));

                    // Run evaluation.
                    System.out.println(" Evaluation for  word = " + targetWordXmlName);
                    //  Evaluation evaluation = new Evaluation();

                    gloveWsdRNN.shouldFailIfUnableToRetrieveExistingModels = true;
                    MultiLayerNetwork net = gloveWsdRNN.multiLayerNetwork(vectorSize, posType, targetWordWithoutExtension(targetWordXmlName), wsdIter);

                    while (wsdIter.hasNext()) {
                        List<String> instanceIds = wsdIter.instanceIds();


                        DataSet t = wsdIter.next();

                        INDArray features = t.getFeatureMatrix();
                        INDArray labels = t.getLabels();
                        INDArray inMask = t.getFeaturesMaskArray();
                        INDArray outMask = t.getLabelsMaskArray();
                        INDArray predicted = net.output(features, false, inMask, outMask);

                        System.out.print(labels.size(0) + " . ");
                        System.out.println(labels.size(1));
                        System.out.print(predicted.size(0) + " . ");
                        System.out.println(predicted.size(1));


                        SensevalOutputFormattedAnswers sensevalFormatOutput = new SensevalOutputFormattedAnswers(wsdIter.getLabels());


                        List<String> formattedOutput = sensevalFormatOutput.evalTimeSeries(instanceIds, labels, predicted, outMask);
//                    System.out.println(formattedOutput);

                        for (String str : formattedOutput) {
                            writer.write(str + "\n");
                        }

                        //evaluation.evalTimeSeries(labels, predicted, outMask);

                    }
                    //System.out.println(evaluation.stats());
                    wsdIter.reset();


                    System.out.println("----- completed : " + posType + "-" + targetWordXmlName + "-----");
                    writer.close();
                } catch (Exception e) {
                    // unable to run test
                    // log and fail
                    try(PrintWriter writer = new PrintWriter(new FileWriter("failure_log_" + targetWordXmlName.replaceAll("/", ".")))) {
                        writer.write("failed!" );
                        e.printStackTrace(writer);
                    }
                }

            }
        }
    }

    private static String targetWordWithoutExtension(String s) {
        return s.split("\\.")[0];
    }
}
