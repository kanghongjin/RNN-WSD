package org.deeplearning4j.examples.word2vec.wsdrnn;

import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InvalidObjectException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

/**
 */
public class GloveWsdRnnTrain extends GloveWsdRNN {

    protected static class TrainingModelRunner {
        public static void run() throws Exception {
            Map<String, List<String>> trainingCases = new GloveWsdRnnTrain().testCases(TRAINING_DATA_PATH);
            Map<String, List<String>> testCases = new GloveWsdRnnTrain().testCases(TEST_DATA_PATH);

            System.out.println(trainingCases);

            final int batchSize = 50;                 // Number of examples in each minibatch
            final int vectorSize = 50;                // Size of the word vectors. 50 in GLOVE. CHANGE THIS WHEN EXPERIMENTING WITH WORD VECTORS
            final int nEpochs = 1000;                    // Number of epochs (full passes of training data) to train on
            final int truncateContextsToLength = 100;  // Truncate reviews with length (# words) greater than this

            WordVectors wordVectors = WordVectorSerializer.loadTxtVectors(
                    new File(WORD_VECTORS_PATH)
            );

            for (Map.Entry<String, List<String>> posTypeAndTargetWordNames : trainingCases.entrySet()) {
                String posType = posTypeAndTargetWordNames.getKey();

                List<String> targetWordsXmlNames = posTypeAndTargetWordNames.getValue();

                for (String targetWordXmlName : targetWordsXmlNames) {

                    System.out.println(targetWordXmlName);

                    // DataSetIterators for training and testing respectively
                    // Using AsyncDataSetIterator to do data loading in a separate thread; this may improve performance vs. waiting for data to load

                    String targetWordXmlPath = posType + "/" + targetWordXmlName;
                    String targetWordKeyPath = targetWordXmlPath.replace("xml", "key");

                    WsdIterator trainIterator = new WsdIterator(TRAINING_DATA_PATH, targetWordXmlPath, targetWordKeyPath,
                            wordVectors, batchSize, truncateContextsToLength);
                    WsdIterator testIterator = null;
                    try {
                        testIterator = new WsdIterator(TEST_DATA_PATH, targetWordXmlPath,
                                targetWordKeyPath, wordVectors,
                                100, truncateContextsToLength);
                    } catch (Exception e) {
                        // silently ignore...
                        // code below this segment must check for nullity of testIterator
                    }
                    if (testIterator != null) {
                        trainIterator.mergeLabels(testIterator);
                        testIterator.mergeLabels(trainIterator);
                    }


                    System.out.println("keysOfInstances:");
                    {
                        List<String> senseLabels = trainIterator.getLabels();
                        for (int senseNumber = 0; senseNumber < senseLabels.size(); senseNumber++) {
                            System.out.print(" id: " + senseNumber + " : " + senseLabels.get(senseNumber) + " , ");
                        }
                        System.out.println();


                        String senseLabelFileName = "senseLabels/" + senseLabelFileName(targetWordXmlPath);
                        if (!new File(senseLabelFileName).exists()) {
                            Files.createDirectories(Paths.get(senseLabelFileName).getParent());
                            Files.createFile(Paths.get(senseLabelFileName));

                        }

                        try(BufferedWriter senseLabelWriter = new BufferedWriter(new FileWriter(senseLabelFileName))) {
                            for (int senseNumber = 0; senseNumber < senseLabels.size(); senseNumber++) {
                                senseLabelWriter.write(senseNumber + "," + senseLabels.get(senseNumber) );
                                senseLabelWriter.newLine();
                            }
                        }
                    }

                    MultiLayerNetwork net = null;
                    try {
                        net = new GloveWsdRnnTrain()
                                .multiLayerNetwork(vectorSize, posType, targetWordXmlName.split("\\.")[0], trainIterator, true);
                    } catch (InvalidObjectException ioe) {
                        System.out.println("SKIP " + targetWordXmlName.split("\\.")[0]);
                        continue;
                    }



                    System.out.println("Starting training epochs num: " + nEpochs);
                    for (int i = 0; i < nEpochs; i++) {
                        net.fit(trainIterator);
                        trainIterator.reset();

                        System.out.println(" Completed training for Epoch " + i + " of word = " + targetWordXmlName);


                        //Run evaluation.
                        if (i % 50 == 0 && testIterator != null) {
                            System.out.println(" Evaluation for Epoch " + i + " of word = " + targetWordXmlName);

                            evaluationForTest(testIterator, net);

                            testIterator.reset();
                        }

                    }

                    System.out.println("----- completed : " + posType + "-" + targetWordXmlName + "-----");

                    new GloveWsdRnnTrain().persistModel(net, posType, targetWordXmlName.split("\\.")[0]);
                }
            }
        }
    }


    private static void evaluationForTest(WsdIterator testIterator, MultiLayerNetwork net) {
        Evaluation evaluation = new Evaluation();

        while (testIterator.hasNext()) {
            DataSet t = testIterator.next();
            INDArray features = t.getFeatureMatrix();
            INDArray labels = t.getLabels();
            INDArray inMask = t.getFeaturesMaskArray();
            INDArray outMask = t.getLabelsMaskArray();
            INDArray predicted = net.output(features, false, inMask, outMask);

            evaluation.evalTimeSeries(labels, predicted, outMask);
        }
        System.out.println(evaluation.stats());
    }

    public static void main(String[] args) throws Exception {
        TrainingModelRunner.run();
    }

}
