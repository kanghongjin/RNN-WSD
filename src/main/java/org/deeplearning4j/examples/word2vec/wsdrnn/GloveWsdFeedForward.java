package org.deeplearning4j.examples.word2vec.wsdrnn;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * TODO do up header comments
 *
 *
 * NOTE / INSTRUCTIONS:
 * Download the Glove vectors manually...
 *
 * @author kanghj
 */
public class GloveWsdFeedForward extends GloveWsdRNN{


    @Override
    public  String getModelPersistanceCoefficientsName(String targetWordPos, String targetWord) {
        return "coefficients_" + targetWordPos + "_" + targetWord;
    }

    @Override
    public  String getModelPersistanceConfName(String targetWordPos, String targetWord) {
        return "conf_" + targetWordPos + "_" + targetWord;
    }


    @Override
    MultiLayerNetwork multiLayerNetwork(int vectorSize, String targetWordPos, String targetWordXmlName, WsdIterator trainIterator) throws Exception {
        MultiLayerNetwork net;

        if (shouldFailIfUnableToRetrieveExistingModels) {
            if (!isModelExists(targetWordPos, targetWordXmlName)) {
                throw new Exception("no model!");
            }

            System.err.println("retriving model for " + targetWordXmlName);
            net = retrieveModel(targetWordPos, targetWordXmlName);

        } else {
            if (!isModelExists(targetWordPos, targetWordXmlName)) {
                System.err.println("making new model for " + targetWordXmlName);
                // Set up new network configuration
                MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1)
                        .updater(Updater.RMSPROP)
                        .regularization(true).l2(1e-5)
                        .weightInit(WeightInit.XAVIER)
                        .gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue).gradientNormalizationThreshold(1.0)
                        .learningRate(0.0018)
                        .list(2)
                        .layer(0, new GravesLSTM.Builder().nIn(vectorSize).nOut(5)
                                .activation("softsign").build())
                        .layer(1, new RnnOutputLayer.Builder().activation("softmax")
                                .lossFunction(LossFunctions.LossFunction.MCXENT).nIn(5).nOut(trainIterator.totalOutcomes()).build())
                        .pretrain(false).backprop(true).build();

                net = new MultiLayerNetwork(conf);
                net.init();
                net.setListeners(new ScoreIterationListener(1));
            } else {
                System.err.println("retriving model for " + targetWordXmlName);
                net = retrieveModel(targetWordPos, targetWordXmlName);
            }
        }
        return net;
    }


    protected static String senseLabelFileName(String targetWordXmlPath) {
        return targetWordXmlPath.replace("xml", "txt").replaceAll("/", ".");
    }

}
