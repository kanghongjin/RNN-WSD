package org.deeplearning4j.examples.word2vec.wsdrnn;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

/**
 * Created by kanghj on 30/1/16.
 */
public interface GloveWsd {
    MultiLayerNetwork multiLayerNetwork(int vectorSize, String targetWordPos, String targetWordXmlName, WsdIterator trainIterator);
}
