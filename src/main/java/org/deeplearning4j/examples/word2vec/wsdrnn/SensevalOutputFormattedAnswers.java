package org.deeplearning4j.examples.word2vec.wsdrnn;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * Not to replace the Evaluation class. But to generally provide coarse grained accuracy.
 */
public class SensevalOutputFormattedAnswers {
    List<String> sortedSenses;

    public SensevalOutputFormattedAnswers(List<String> sortedSenses) {
        this.sortedSenses = sortedSenses;

    }

    int numCorrect = 0;
    int numTotalCases = 0;

    public List<String> evalTimeSeries(List<String> instanceIds, INDArray labels, INDArray predicted, INDArray outputMask) throws Exception {

        int totalOutputExamples = outputMask.sumNumber().intValue();
        int outSize = labels.size(1);

        INDArray labels2d = Nd4j.create(totalOutputExamples, outSize);
        INDArray predicted2d = Nd4j.create(totalOutputExamples,outSize);

        int rowCount = 0;
        for ( int ex=0; ex<outputMask.size(0); ex++ ) {
            //System.out.println("t values: " + outputMask.size(1));
            for ( int t=0; t<outputMask.size(1); t++ ) {
                if (outputMask.getDouble(ex,t) == 0.0) continue;

                System.out.println(rowCount + ", " + ex + " , " + t);
                System.out.println(predicted.size(0) + ", " + predicted.size(1) + ", " + predicted.size(2));
                System.out.println(predicted2d.size(0) + ", " + predicted2d.size(1) );

                System.out.println("-----");
                System.out.flush();

                labels2d.putRow(rowCount, labels.get(NDArrayIndex.point(ex), NDArrayIndex.all(), NDArrayIndex.point(t)));
                predicted2d.putRow(rowCount, predicted.get(NDArrayIndex.point(ex), NDArrayIndex.all(), NDArrayIndex.point(t)));

                rowCount++;
            }
        }

        // a typical output looks like : bnc_ABM_33 authority.40003 authority%1:07:00::
        // i.e. docId instanceId senseId

        // we'll use a dummy for docId, since it's not used in our workflow
        final String dummyDocumentId = "dummy";


        if (labels2d.rows() != predicted2d.rows() ) {
            System.err.println(predicted2d.rows());
            System.err.println(labels2d.rows());
            System.err.println("======");
            throw new Exception("different number of rows");
        }

        List<String> result = resultsFromLabelsAndPredicted(instanceIds, labels2d, predicted2d, dummyDocumentId);


        return result;

    }

    private List<String> resultsFromLabelsAndPredicted(List<String> instanceIds, INDArray labels2d, INDArray predicted2d, String dummyDocumentId) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < labels2d.rows(); i++) {
            INDArray currRow = labels2d.getRow(i);
            INDArray guessRow = predicted2d.getRow(i);

            int currMax;
            {
                double max = currRow.getDouble(0);
                currMax = 0;
                for (int col = 1; col < currRow.columns(); col++) {
                    if (currRow.getDouble(col) > max) {
                        max = currRow.getDouble(col);
                        currMax = col;
                    }
                }
            }
            int guessMax;
            {
                double max = guessRow.getDouble(0);
                guessMax = 0;
                for (int col = 1; col < guessRow.columns(); col++) {
                    if (guessRow.getDouble(col) > max) {
                        max = guessRow.getDouble(col);
                        guessMax = col;
                    }
                }
            }

            this.numTotalCases += 1;
            if (currMax == guessMax) {
                this.numCorrect += 1;
            }

            StringJoiner row = new StringJoiner(" ");
            row.add(dummyDocumentId)
                .add(instanceIds.get(i))
                .add(this.sortedSenses.get(guessMax));

            result.add(row.toString());

        }

        return result;
    }


    public String stats() {
        return Integer.toString(this.numCorrect) + " out of " + Integer.toString(this.numTotalCases);
    }


}
