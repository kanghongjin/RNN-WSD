package org.deeplearning4j.examples.word2vec.wsdrnn;

import org.apache.commons.io.FilenameUtils;
import org.deeplearning4j.datasets.iterator.DataSetIterator;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.text.tokenization.tokenizer.preprocessor.CommonPreprocessor;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.DataSetPreProcessor;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/** This is a DataSetIterator that is specialized for the Senseval format used in the Senseval2 and Senseval3 example.
 * This Iterate is used for a *single* target word.
 * It takes either the train or test set data from this data set, plus a WordVectors object (https://code.google.com/p/word2vec/)
 * and generates training data sets.<br>
 * Inputs/features: variable-length time series, where each word (with unknown words removed) is represented by
 * its Word2Vec vector representation.<br>
 * Labels/target: a single class, predicted at the final time step (word) of context window
 *
 */
public class WsdIterator implements DataSetIterator {
    private final WordVectors wordVectors;

    private final int batchSize;
    private final int vectorSize;
    private final int truncateLength;

    private int cursor = 0;
    private final File keyFile;

    private List<String> contextInstances = new ArrayList<>();
    protected List<String> keysOfInstances = new ArrayList<>();
    private List<String> sortedPossibleKeys = new ArrayList<>();

    private int numInstances = 0;

    private List<String> instanceIds = new ArrayList<>();



    private final TokenizerFactory tokenizerFactory;


    public WsdIterator(String dataDirectory, String targetWordXmlLocation, String targetWordKeyLocation,
                       WordVectors wordVectors,
                       int batchSize, int truncateLength) throws Exception {
        this(dataDirectory, targetWordXmlLocation, targetWordKeyLocation, wordVectors, batchSize, truncateLength, "");
    }
    /**
     * @param dataDirectory the directory of the data set
     * @param targetWordXmlLocation
     * @param wordVectors WordVectors object
     * @param batchSize Size of each minibatch for training
     * @param truncateLength If context exceed
     * @param senseLabelsLocation location of senselabel text file
     */
    public WsdIterator(String dataDirectory, String targetWordXmlLocation, String targetWordKeyLocation,
                       WordVectors wordVectors,
                       int batchSize, int truncateLength, String senseLabelsLocation) throws Exception {
        this.batchSize = batchSize;
        this.vectorSize = wordVectors.lookupTable().layerSize();

        File instanceFile = new File(FilenameUtils.concat(dataDirectory, targetWordXmlLocation));
        this.keyFile        = new File( FilenameUtils.concat(dataDirectory, targetWordKeyLocation ) );

        if (!instanceFile.exists() || !this.keyFile.exists()) {
            throw new Exception("either no such instance or no such key");
        }

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        DocumentBuilder dBuilder = null;
        Document doc = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(instanceFile);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        // see http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work if needed
        //doc.getDocumentElement().normalize();

        NodeList lexeltNodeList = doc.getElementsByTagName("lexelt");
        if (lexeltNodeList.getLength() != 1) {
            throw new Exception("Invalid number of lexelt: only 1 lexelt per file supported now!");
        }

        for (int i = 0; i < lexeltNodeList.getLength(); i++) {
            Node lexeltNode = lexeltNodeList.item(i);

            // get instances
            if (lexeltNode.getNodeType() == Node.ELEMENT_NODE) {
                Element lexeltElement = (Element) lexeltNode;

                NodeList instances = lexeltElement.getElementsByTagName("instance");

                for (int j = 0; j < instances.getLength(); j++) {
                    Node instance = instances.item(j);

                    Element instanceElement = (Element)instance;

                    this.instanceIds.add(((Element) instance).getAttributes().getNamedItem("id").getNodeValue());


                    Node context = instanceElement.getElementsByTagName("context").item(0);

                    Node head = ((Element)context).getElementsByTagName("head").item(0);

                    String headText = ((Element)context).getElementsByTagName("head").item(0).getTextContent();

                    int startIndex = head.getPreviousSibling().getTextContent().lastIndexOf(".");
                    if (startIndex == -1) {
                        startIndex = -1; // take first position (+1 to 0) later. explicitly mentioning it here to prevent confusion
                    }
                    int endIndex = head.getNextSibling().getTextContent().indexOf(".");
                    if (endIndex == -1) {
                        endIndex = head.getNextSibling().getTextContent().length() - 1; // take last position
                    }

                    String surroundingText = head.getPreviousSibling().getTextContent().substring(startIndex + 1)
                                            + headText
                                            + head.getNextSibling().getTextContent().substring(0, endIndex + 1);

                    this.contextInstances.add(surroundingText);
                }

                this.numInstances += instances.getLength();
            }
        }

        this.keysOfInstances = initialiseKeysOfInstances();
        if (senseLabelsLocation.isEmpty() ) {
            initPossibleLabelsFromKey(this.keysOfInstances);
        } else {
            initPossibleLabelsFromSenseLabelFile(senseLabelsLocation);
        }

        this.wordVectors = wordVectors;
        this.truncateLength = truncateLength;

        tokenizerFactory = new DefaultTokenizerFactory();
        tokenizerFactory.setTokenPreProcessor(new CommonPreprocessor());
    }

    private void initPossibleLabelsFromSenseLabelFile(String senseLabelsLocation) throws IOException {
        try(BufferedReader inputReader =  new BufferedReader(new FileReader(senseLabelsLocation))) {

            List<String> senses = new ArrayList<>();

            String line;
            while ((line = inputReader.readLine()) != null) {
                String key = line.split(",")[1];
                senses.add(key);
            }

            Collections.sort(senses);

            this.sortedPossibleKeys = new ArrayList<>(senses);

        }

    }

    private void initPossibleLabelsFromKey(List<String> keysOfInstances) throws IOException {

        Set<String> uniqueSenses = new HashSet<>(keysOfInstances);

        this.sortedPossibleKeys = Arrays.asList(uniqueSenses.toArray()).stream()
                                        .map(element->(String) element)
                                        .collect(Collectors.toList());
        Collections.sort(this.sortedPossibleKeys);


        System.out.println("LABEL SIZE IN WSDITERATOR: " + this.sortedPossibleKeys.size());
    }

    private List<String> initialiseKeysOfInstances() throws IOException {
        String lineInKeyFile;
        BufferedReader inputReader =  new BufferedReader(new FileReader(this.keyFile));

        List<String> result = new ArrayList<>();
        while ((lineInKeyFile = inputReader.readLine()) != null) {
            result.add(lineInKeyFile.split(" ")[2]);

        }
        inputReader.close();

        return result;
    }

    public <K extends WsdIterator> void mergeLabels(K other) {
        Set<String> uniqueSenses = new HashSet<>(this.keysOfInstances);
        System.out.println(uniqueSenses);
        uniqueSenses.addAll(new HashSet<>(other.keysOfInstances));
        System.out.println(uniqueSenses);


        this.sortedPossibleKeys = Arrays.asList(uniqueSenses.toArray())
                                        .stream()
                                        .map(element->(String) element)
                                        .collect(Collectors.toList());

        Collections.sort(this.sortedPossibleKeys);



    }

    public List<String> instanceIds() {

        return new ArrayList<>(this.instanceIds.subList(cursor, Math.min(cursor + batchSize, this.numInstances )));
    }


    @Override
    public DataSet next(int num) {
        if (cursor >= this.numInstances ) throw new NoSuchElementException();
        try {
            return nextDataSet(num);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private DataSet nextDataSet(int num) throws IOException {

        List<String> contexts = new ArrayList<>(num);
        int[] senseId = new int[num];

        for (int i = 0; i < num && cursor < totalExamples(); i++ ) {
            int posReviewNumber = cursor ;

            // next instance
            String contextText = this.contextInstances.get(posReviewNumber);
            contexts.add(contextText);
            String whichKey = this.keysOfInstances.get(posReviewNumber);
            senseId[i] = this.sortedPossibleKeys.indexOf(whichKey);

            cursor++;
        }

        //Second: tokenize reviews and filter out unknown words
        List<List<String>> allTokens = new ArrayList<>(contexts.size());
        int maxLength = 0;
        for (String s : contexts) {
            List<String> tokens = tokenizerFactory.create(s).getTokens();
            List<String> tokensFiltered = new ArrayList<>();
            for (String token : tokens ) {
                if (wordVectors.hasWord(token)) tokensFiltered.add(token);
            }
            allTokens.add(tokensFiltered);
            maxLength = Math.max(maxLength, tokensFiltered.size());
        }

        //If longest review exceeds 'truncateLength': only take the first 'truncateLength' words
        if (maxLength > truncateLength) {
            maxLength = truncateLength;
        }

        // Create data for training
        // Here, we have contexts.size() examples of varying lengths
        INDArray features = Nd4j.create(contexts.size(), vectorSize, maxLength);
        INDArray labels = Nd4j.create(contexts.size(), this.sortedPossibleKeys.size(), maxLength);    //num labels = num senses

        //Because we are dealing with reviews of different lengths and only one output at the final time step: use padding arrays
        //Mask arrays contain 1 if data is present at that time step for that example, or 0 if data is just padding
        INDArray featuresMask = Nd4j.zeros(contexts.size(), maxLength);
        INDArray labelsMask = Nd4j.zeros(contexts.size(), maxLength);

        int[] temp = new int[2];
        for (int i = 0; i < contexts.size(); i++ ) {
            List<String> tokens = allTokens.get(i);

            temp[0] = i;
            // Get word vectors for each word in the context, and put them in the training data
            for (int j=0; j < tokens.size() && j < maxLength; j++ ) {
                String token = tokens.get(j);
                INDArray vector = wordVectors.getWordVectorMatrix(token);
                features.put(new INDArrayIndex[] {NDArrayIndex.point(i), NDArrayIndex.all(), NDArrayIndex.point(j)},
                             vector);

                temp[1] = j;
                featuresMask.putScalar(temp, 1.0);  //Word is present (not padding) for this example + time step -> 1.0 in features mask
            }

            int idx = senseId[i];
            int lastIdx = Math.min(tokens.size(), maxLength);
            labels.putScalar(new int[]{i,idx,lastIdx-1}, 1.0);   // Set label
            labelsMask.putScalar(new int[]{i,lastIdx-1}, 1.0);   // Specify that an output exists at the final time step for this example
            //System.out.println(i + "  -> " + (lastIdx-1));
            //System.out.println(tokens);
            //System.out.println("sense idx is " + idx);
        }

        return new DataSet(features, labels, featuresMask, labelsMask);
    }

    @Override
    public int totalExamples() {
        return this.numInstances ;
    }

    @Override
    public int inputColumns() {
        return vectorSize;
    }

    @Override
    public int totalOutcomes() {

        return this.sortedPossibleKeys.size();
    }

    @Override
    public void reset() {
        cursor = 0;
    }

    @Override
    public int batch() {
        return batchSize;
    }

    @Override
    public int cursor() {
        return cursor;
    }

    @Override
    public int numExamples() {
        return totalExamples();
    }

    @Override
    public void setPreProcessor(DataSetPreProcessor preProcessor) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> getLabels() {
        return new ArrayList<>(this.sortedPossibleKeys);
    }

    @Override
    public boolean hasNext() {
        return cursor < numExamples();
    }

    @Override
    public DataSet next() {
        return next(batchSize);
    }

    @Override
    public void remove() {

    }

}
