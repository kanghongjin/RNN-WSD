package org.deeplearning4j.examples.word2vec.wsdrnn;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.deeplearning4j.datasets.iterator.AsyncDataSetIterator;
import org.deeplearning4j.datasets.iterator.DataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.embeddings.wordvectors.WordVectors;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.Updater;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

/**
 *
 * TODO do up header comments
 *
 *
 * NOTE / INSTRUCTIONS:
 * Download the Glove vectors manually...
 *
 * @author kanghj
 */
public class GloveWsdRNN {


    /** Location to save and extract the training/testing data */
    public static final String TRAINING_DATA_PATH = //"/media/kanghj/4AA0EB67A0EB5849/Users/user/Downloads/one-million-sense-tagged-instances-wn30.tar/one-million-sense-tagged-instances-wn30/";
            //System.getProperty("user.home") + "/ims_0.9.2/split-senseval-2-with-rnn-error/";
            System.getProperty("user.home") + "/ims_0.9.2/onemillion/";
    public static final String TEST_DATA_PATH =
            //System.getProperty("user.home") + "/ims_0.9.2/split-senseval2-test/";
            System.getProperty("user.home") + "/ims_0.9.2/onemillion/";


    /** Location (local file system) for the Word Vectors. Set this manually. */
    public static final String WORD_VECTORS_PATH = System.getProperty("user.home") + "/ims_0.9.2/glove/vectors.6B.50d.txt";

    /** Location (local file system) for the saved models (one for each word). Set this manually. */
    public static final String MODEL_PATH = "models/";

    public static boolean shouldFailIfUnableToRetrieveExistingModels = false;

    Map<String, List<String>> testCases(String dataDirectoryLocation) throws Exception {
        Map<String, List<String>> result = new HashMap<>();

        File rootDirectory = new File(dataDirectoryLocation);
        System.out.println(dataDirectoryLocation);
        File[] categories = rootDirectory.listFiles();
        System.out.println(Arrays.asList(categories ));

        if (categories == null || categories.length == 0) {
            throw new FileNotFoundException("unable to process training/test files");
        }


        for (File categoryDirectory : categories) {
            File[] targetWordFiles = categoryDirectory.listFiles();
            System.out.println(Arrays.asList(targetWordFiles ));

            // support the different formats. in one-million-senses, e.g. noun-> word.xml
            //                                  in split-senseval, e.g. n -> word -> training_data.xml
            List<String> xmlFileNames = new ArrayList<>();

            for (File targetWordFile : targetWordFiles) {

                if (targetWordFile.isDirectory()) {
                    File[] xmlFilesInDirectory = targetWordFile.listFiles(
                                                    (dir, name) -> (FilenameUtils.getExtension(name).equals("xml"))
                                                    );


                    xmlFileNames.addAll(
                            Arrays.asList(xmlFilesInDirectory).stream()
                                                                .map(file -> targetWordFile.getName() + "/" + file.getName())
                                                                .collect(Collectors.toList())

                                            );


                } else if (FilenameUtils.getExtension(targetWordFile.getName()).equals("xml")) {
                    xmlFileNames.add(targetWordFile.getName());
                }

            }

            result.put(categoryDirectory.getName(), xmlFileNames);

        }

        if (result.isEmpty()) {
            throw new Exception("Initialisation of category -> files failed");
        }

        return result;
    }

    public void persistModel(MultiLayerNetwork net, String targetWordPos, String targetWord) throws IOException {

        File directory = new File(MODEL_PATH);
        if (!directory.exists()) {
            directory.mkdirs();
        }

        // Write the network parameters:
        System.out.println(targetWord);
        System.out.println(MODEL_PATH + getModelPersistanceCoefficientsName(targetWordPos, targetWord) + ".bin");
        File coeffFile = new File(MODEL_PATH + getModelPersistanceCoefficientsName(targetWordPos, targetWord + ".bin"));
        if (!coeffFile.exists()) {
            coeffFile.createNewFile();
        }
        OutputStream fos = Files.newOutputStream(coeffFile.toPath());
        DataOutputStream dos = new DataOutputStream(fos);
        Nd4j.write(net.params(), dos);
        dos.flush();
        dos.close();

        // Write the network configuration
        FileUtils.write(new File(MODEL_PATH + getModelPersistanceConfName(targetWordPos,targetWord ) + ".json"),
                                net.getLayerWiseConfigurations().toJson());
    }

    /**
     * Return true if the both the conf and coefficient files for the pos type and word is saved. The {@code targetWord] is stripped of any
     * dots (.) to handle filenames.
     * @param targetWordPos
     * @param targetWord
     * @return
     */
    protected boolean isModelExists(String targetWordPos, String targetWordXmlName) {
        String targetWord = targetWordXmlName.split("\\.")[0];

        File coeffFile = new File(MODEL_PATH + getModelPersistanceCoefficientsName(targetWordPos, targetWord) + ".bin");
        System.err.println("looking for " + MODEL_PATH + getModelPersistanceCoefficientsName(targetWordPos, targetWord) + ".bin");
        File confFile = new File(MODEL_PATH + getModelPersistanceConfName(targetWordPos,targetWord ) + ".json");
        System.err.println("looking for " + MODEL_PATH + getModelPersistanceConfName(targetWordPos,targetWord ) + ".json");
        return coeffFile.exists() && confFile.exists();
    }

    String getModelPersistanceCoefficientsName(String targetWordPos, String targetWord) {
        return "coefficients_" + targetWordPos + "_" + targetWord;
    }

    String getModelPersistanceConfName(String targetWordPos, String targetWord) {
        return "conf_" + targetWordPos + "_" + targetWord;
    }

    protected MultiLayerNetwork retrieveModel(String targetWordPos, String targetWord) throws IOException {

        // Load network configuration from disk
        MultiLayerConfiguration confFromJson = MultiLayerConfiguration.fromJson(
                                                    FileUtils.readFileToString(
                                                            new File(MODEL_PATH + getModelPersistanceConfName(targetWordPos,targetWord ) + ".json")
                                                    )
                                                );

        // Load parameters from disk
        DataInputStream dis = new DataInputStream(
                                    new FileInputStream(MODEL_PATH + getModelPersistanceCoefficientsName(targetWordPos, targetWord) + ".bin")
                                    );
        INDArray newParams = Nd4j.read(dis);
        dis.close();

        // Create a MultiLayerNetwork from the saved configuration and parameters
        MultiLayerNetwork savedNetwork = new MultiLayerNetwork(confFromJson);
        savedNetwork.init();
        savedNetwork.setParameters(newParams);

        return savedNetwork;
    }

    MultiLayerNetwork multiLayerNetwork(int vectorSize, String targetWordPos, String targetWordXmlName, WsdIterator trainIterator) throws Exception {
        return multiLayerNetwork(vectorSize, targetWordPos, targetWordXmlName, trainIterator, false);
    }
    MultiLayerNetwork multiLayerNetwork(int vectorSize, String targetWordPos, String targetWordXmlName, WsdIterator trainIterator, boolean skipIfGotModel) throws Exception {
        MultiLayerNetwork net;

        if (skipIfGotModel) {
            if (isModelExists(targetWordPos, targetWordXmlName)) {
                throw new InvalidObjectException("got model!");
            }
        }

        if (shouldFailIfUnableToRetrieveExistingModels) {
            if (!isModelExists(targetWordPos, targetWordXmlName)) {
                throw new Exception("no model!");
            }

            System.err.println("retriving model for " + targetWordXmlName);
            net = retrieveModel(targetWordPos, targetWordXmlName);

        } else {
            if (!isModelExists(targetWordPos, targetWordXmlName)) {
                System.err.println("making new model for " + targetWordXmlName);
                // Set up new network configuration
                MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).iterations(1)
                        .updater(Updater.RMSPROP)
                        .regularization(true).l2(1e-5)
                        .weightInit(WeightInit.XAVIER)
                        .gradientNormalization(GradientNormalization.ClipElementWiseAbsoluteValue).gradientNormalizationThreshold(1.0)
                        .learningRate(0.0018)
                        .list(2)
                        .layer(0, new GravesLSTM.Builder().nIn(vectorSize).nOut(5)
                                .activation("softsign").build())
                        .layer(1, new RnnOutputLayer.Builder().activation("softmax")
                                .lossFunction(LossFunctions.LossFunction.MCXENT).nIn(5).nOut(trainIterator.totalOutcomes()).build())
                        .pretrain(false).backprop(true).build();

                net = new MultiLayerNetwork(conf);
                net.init();
                net.setListeners(new ScoreIterationListener(1));
            } else {
                System.err.println("retriving model for " + targetWordXmlName);
                net = retrieveModel(targetWordPos, targetWordXmlName);
            }
        }
        return net;
    }


    protected static String senseLabelFileName(String targetWordXmlPath) {
        return targetWordXmlPath.replace("xml", "txt").replaceAll("/", ".");
    }

}
